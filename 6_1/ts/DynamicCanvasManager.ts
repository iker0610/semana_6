import {arrayEquals} from "./utils.js"

type Point = [x: number, y: number]
type Size = [width: number, y: number]

export class DynamicCanvasManager {
    // Atributos relativos a los Canvas
    private readonly spriteSheetCanvas: HTMLCanvasElement = document.getElementById('SpriteSheetCanvas') as HTMLCanvasElement
    private readonly spriteSheetCtx: CanvasRenderingContext2D

    private readonly selectorCanvas: HTMLCanvasElement = document.getElementById('SelectorCanvas') as HTMLCanvasElement
    private readonly selectorCtx: CanvasRenderingContext2D

    private readonly spritePreviewCanvas: HTMLCanvasElement = document.getElementById('SpritePreviewCanvas') as HTMLCanvasElement
    private readonly spritePreviewCtx: CanvasRenderingContext2D

    // Atributos relativos al selector
    private readonly maxXPosition: number
    private readonly maxYPosition: number

    private readonly selectorSize: Size
    private _selectorCoordinates: Point = [-1, -1]

    private readonly selectorColor: string


    constructor(
        background_img: HTMLImageElement,
        selector_size: Size,
        selector_start_point: Point = [0, 0],
        selector_color: string      = "#0255b0",
    ) {
        // Configuramos los tamaños de los canvas y del contenedor principal
        const mainCanvasContainer = document.getElementById('SpriteCanvasContainer') as HTMLDivElement
        mainCanvasContainer.setAttribute(
            "style",
            `width:${background_img.width}px; height:${background_img.height}px;`,
        )

        this.spriteSheetCanvas.width = background_img.width
        this.spriteSheetCanvas.height = background_img.height

        this.selectorCanvas.width = background_img.width
        this.selectorCanvas.height = background_img.height

        this.spritePreviewCanvas.width = selector_size[0] * 2
        this.spritePreviewCanvas.height = selector_size[1] * 2

        // Obtenemos los contextos de los canvas
        this.spriteSheetCtx = this.spriteSheetCanvas.getContext('2d')!
        this.selectorCtx = this.selectorCanvas.getContext('2d')!
        this.spritePreviewCtx = this.spritePreviewCanvas.getContext('2d')!

        // Aplicamos la imagen de base
        this.spriteSheetCtx.drawImage(background_img, 0, 0)

        // Aplicamos estilos predeterminados al canvas del selector
        this.selectorColor = selector_color
        this.selectorCtx.strokeStyle = this.selectorColor
        this.selectorCtx.fillStyle = this.selectorColor
        this.selectorCtx.font = "bold 11px Montserrat"
        this.selectorCtx.textAlign = "right"
        this.selectorCtx.textBaseline = "top"


        // Configuramos la máxima anchura y altura
        this.maxXPosition = background_img.width - selector_size[0]
        this.maxYPosition = background_img.height - selector_size[1]

        if (background_img.width < selector_size[0] || background_img.height < selector_size[1])
            throw new RangeError("El tamaño del selector tiene que ser menor que el de la imagen")
        else this.selectorSize = selector_size

        this.selectorCoordinates = selector_start_point
    }

    private get selectorCoordinates(): Point {return this._selectorCoordinates}

    private set selectorCoordinates(selector_point: Point) {
        // Desestructuramos el parámetro
        let [x, y] = selector_point

        // Guardamos los valores del punto original
        const [original_x, original_y] = this.selectorCoordinates

        // En caso de que los nuevos valores sean más grandes que el canvas se establecen al valor topo
        if (this.maxXPosition < x) x = this.maxXPosition
        if ((this.maxYPosition) < y) y = this.maxYPosition
        if (x < 0) x = 0
        if (y < 0) y = 0
        // Actualizamos las coordenadas del selector
        this._selectorCoordinates = [x, y]

        // Solo en caso de que haya habido variación en las coordenadas se actualizarán los canvas
        if (!arrayEquals([original_x, original_y], this.selectorCoordinates)) {
            this.updateCanvas()
        }
    }

    moveSelector(x_offset: number, y_offset: number) {
        const [original_x, original_y] = this.selectorCoordinates
        this.selectorCoordinates = [original_x + x_offset, original_y + y_offset]
    }

    private updateCanvas() {
        // Limpiamos el canvas con el selector
        this.selectorCtx.clearRect(0, 0, this.selectorCanvas.width, this.selectorCanvas.height)

        // Dibujamos el selector
        this.drawSelector()

        // Dibujamos las coordenadas del selector
        this.drawSelectorCoordinates()

        // Dibujamos el fragmento del SpriteSheet en la preview
        this.drawPreview()
    }

    private drawSelector() {
        const ctx = this.selectorCtx
        const [x, y] = this.selectorCoordinates

        ctx.beginPath()
        ctx.moveTo(x, y)
        ctx.lineTo(x, y + this.selectorSize[1])
        ctx.lineTo(x + this.selectorSize[0], y + this.selectorSize[1])
        ctx.lineTo(x + this.selectorSize[0], y)
        ctx.closePath()

        ctx.stroke()
    }

    private drawSelectorCoordinates() {
        const [x, y] = this.selectorCoordinates

        this.selectorCtx.fillText(`Coordinates ( ${x} , ${y} )`, this.selectorCanvas.width - 10, 50)
    }

    private drawPreview() {
        const [x, y] = this.selectorCoordinates
        const [width, height] = this.selectorSize

        this.spritePreviewCtx.drawImage(this.spriteSheetCanvas, x, y, width, height, 0, 0, width * 2, height * 2)
    }
}