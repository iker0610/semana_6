type KeyEventHandler = () => void

//--------------------------------------------------------
//--------------       EVENT HANDLERS       --------------
class KeyboardManager {
    private readonly onArrowUp: KeyEventHandler
    private readonly onArrowDown: KeyEventHandler
    private readonly onArrowRight: KeyEventHandler
    private readonly onArrowLeft: KeyEventHandler

    constructor(
        onArrowUp: KeyEventHandler,
        onArrowDown: KeyEventHandler,
        onArrowRight: KeyEventHandler,
        onArrowLeft: KeyEventHandler,
    ) {
        this.onArrowUp = onArrowUp
        this.onArrowDown = onArrowDown
        this.onArrowRight = onArrowRight
        this.onArrowLeft = onArrowLeft
    }

    handleEvent(event: KeyboardEvent) {
        switch (event.key) {
            // Keys que nos interesan
            case "ArrowUp":
                this.onArrowUp()
                break

            case "ArrowDown":
                this.onArrowDown()
                break

            case "ArrowRight":
                this.onArrowRight()
                break

            case "ArrowLeft":
                this.onArrowLeft()
                break

            default:
                return
        }

        event.preventDefault()
    }
}

//--------------------------------------------------------
//--------------          LOADERS           --------------

function loadImage(url: string): Promise<HTMLImageElement> {
    return new Promise((resolve, reject) => {
        const img = new Image()
        img.onload = () => resolve(img)
        img.onerror = () => reject()
        img.src = url
    })
}


//--------------------------------------------------------
//----------   BASIC UTILITY JS DOES NOT HAVE  -----------

const arrayEquals = (a: any[], b: any[]): Boolean => a.length === b.length && a.every((v, i) => v === b[i])


//--------------------------------------------------------
export {arrayEquals, KeyboardManager, loadImage}