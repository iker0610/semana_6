import { arrayEquals } from "./utils.js";
export class DynamicCanvasManager {
    constructor(background_img, selector_size, selector_start_point = [0, 0], selector_color = "#0255b0") {
        // Inicializamos las coordenadas (esta variable es la que guardará las coordenadas correctas del selector)
        this._selectorCoordinates = [-1, -1];

        // Atributos relativos a los Canvas
        this.spriteSheetCanvas = document.getElementById('SpriteSheetCanvas');
        this.selectorCanvas = document.getElementById('SelectorCanvas');
        this.spritePreviewCanvas = document.getElementById('SpritePreviewCanvas');

        // Configuramos los tamaños de los canvas y del contenedor principal
        const mainCanvasContainer = document.getElementById('SpriteCanvasContainer');
        mainCanvasContainer.setAttribute("style", `width:${background_img.width}px; height:${background_img.height}px;`);

        this.spriteSheetCanvas.width = background_img.width;
        this.spriteSheetCanvas.height = background_img.height;

        this.selectorCanvas.width = background_img.width;
        this.selectorCanvas.height = background_img.height;

        this.spritePreviewCanvas.width = selector_size[0] * 2;
        this.spritePreviewCanvas.height = selector_size[1] * 2;

        // Obtenemos los contextos de los canvas
        this.spriteSheetCtx = this.spriteSheetCanvas.getContext('2d');
        this.selectorCtx = this.selectorCanvas.getContext('2d');
        this.spritePreviewCtx = this.spritePreviewCanvas.getContext('2d');

        // Aplicamos la imagen de base
        this.spriteSheetCtx.drawImage(background_img, 0, 0);

        // Aplicamos estilos predeterminados al canvas del selector
        this.selectorColor = selector_color;
        this.selectorCtx.strokeStyle = this.selectorColor;
        this.selectorCtx.fillStyle = this.selectorColor;
        this.selectorCtx.font = "bold 11px Montserrat";
        this.selectorCtx.textAlign = "right";
        this.selectorCtx.textBaseline = "top";

        // Configuramos la máxima anchura y altura
        this.maxXPosition = background_img.width - selector_size[0];
        this.maxYPosition = background_img.height - selector_size[1];

        // Comprobamos que el tamaño del selector sea válido
        if (background_img.width < selector_size[0] || background_img.height < selector_size[1])
            throw new RangeError("El tamaño del selector tiene que ser menor que el de la imagen");
        else
            this.selectorSize = selector_size;

        // Establecemos las coordenadas iniciales del selector
        this.selectorCoordinates = selector_start_point;
    }

    // Atributo selectorCoordinates con setter personalizado
    get selectorCoordinates() { return this._selectorCoordinates; }
    set selectorCoordinates(selector_point) {
        // Desestructuramos el parámetro
        let [x, y] = selector_point;

        // Guardamos los valores del punto original
        const [original_x, original_y] = this.selectorCoordinates;

        // En caso de que los nuevos valores sean más grandes que el canvas se establecen al valor topo
        if (this.maxXPosition < x)
            x = this.maxXPosition;
        if ((this.maxYPosition) < y)
            y = this.maxYPosition;
        if (x < 0)
            x = 0;
        if (y < 0)
            y = 0;

        // Actualizamos las coordenadas del selector
        this._selectorCoordinates = [x, y];

        // Solo en caso de que haya habido variación en las coordenadas se actualizarán los canvas
        if (!arrayEquals([original_x, original_y], this.selectorCoordinates)) {
            this.updateCanvas();
        }
    }

    // Método para el handler
    moveSelector(x_offset, y_offset) {
        const [original_x, original_y] = this.selectorCoordinates;
        this.selectorCoordinates = [original_x + x_offset, original_y + y_offset];
    }

    // Método que redibuja el canvas entero
    updateCanvas() {
        // Limpiamos el canvas con el selector
        this.selectorCtx.clearRect(0, 0, this.selectorCanvas.width, this.selectorCanvas.height);

        // Dibujamos el selector
        this.drawSelector();
        // Dibujamos las coordenadas del selector
        this.drawSelectorCoordinates();

        // Dibujamos el fragmento del SpriteSheet en la preview
        this.drawPreview();
    }

    // Método para dibujar el selector según los datos actuales
    drawSelector() {
        // Guardamos los datos en unas variables que se manejan mejor
        const ctx = this.selectorCtx;
        const [x, y] = this.selectorCoordinates;

        // Dibujamos el cuadrado
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(x, y + this.selectorSize[1]);
        ctx.lineTo(x + this.selectorSize[0], y + this.selectorSize[1]);
        ctx.lineTo(x + this.selectorSize[0], y);
        ctx.closePath();

        ctx.stroke();
    }

    // Método para dibujar las coordenadas
    drawSelectorCoordinates() {
        const [x, y] = this.selectorCoordinates;
        this.selectorCtx.fillText(`Coordinates ( ${x} , ${y} )`, this.selectorCanvas.width - 10, 50);
    }

    // Método para dibujar el fragmento seleccionado escalado x2 en el otro canvas
    drawPreview() {
        const [x, y] = this.selectorCoordinates;
        const [width, height] = this.selectorSize;
        this.spritePreviewCtx.drawImage(this.spriteSheetCanvas, x, y, width, height, 0, 0, width * 2, height * 2);
    }
}
