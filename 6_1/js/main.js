import {KeyboardManager, loadImage} from "./utils.js";
import {DynamicCanvasManager} from "./DynamicCanvasManager.js";

const SPRITE_SHEET_URL = '/assets/spritesheet.png';
loadImage(SPRITE_SHEET_URL)
    .then(img => {
        const canvasManager = new DynamicCanvasManager(img, [28, 36], [1, 2]);
        window.addEventListener('keydown', new KeyboardManager(() => canvasManager.moveSelector(0, -1), () => canvasManager.moveSelector(0, 1), () => canvasManager.moveSelector(1, 0), () => canvasManager.moveSelector(-1, 0)));
    });
