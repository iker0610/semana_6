//--------------------------------------------------------
//--------------       EVENT HANDLERS       --------------
class KeyboardManager {
    constructor(onArrowUp, onArrowDown, onArrowRight, onArrowLeft) {
        this.onArrowUp = onArrowUp;
        this.onArrowDown = onArrowDown;
        this.onArrowRight = onArrowRight;
        this.onArrowLeft = onArrowLeft;
    }

    handleEvent(event) {
        switch (event.key) {
            // Keys que nos interesan
            case "ArrowUp":
                this.onArrowUp();
                break;
            case "ArrowDown":
                this.onArrowDown();
                break;
            case "ArrowRight":
                this.onArrowRight();
                break;
            case "ArrowLeft":
                this.onArrowLeft();
                break;
            default:
                return;
        }
        event.preventDefault();
    }
}

//--------------------------------------------------------
//--------------          LOADERS           --------------
function loadImage(url) {
    return new Promise((resolve, reject) => {
        const img = new Image();
        img.onload = () => resolve(img);
        img.onerror = () => reject();
        img.src = url;
    });
}

//--------------------------------------------------------
//----------   BASIC UTILITY JS DOES NOT HAVE  -----------
const arrayEquals = (a, b) => a.length === b.length && a.every((v, i) => v === b[i]);

//--------------------------------------------------------
export {arrayEquals, KeyboardManager, loadImage};
