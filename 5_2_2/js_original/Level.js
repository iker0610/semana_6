import {Matrix} from './math.js';

export default class Level {
    constructor() {
        this.tiles = new Matrix();
        this.background = null;
    }

    draw(context, offset) {
        /*
        Ejercicio 9. Tema 5: Canvas
        dibujar en el contexto la imagen de background con el
        desplazamiento indicado en el parámetro offset
        (recuerda que el contexto tiene unas dimensiones de 300x300)
        */

        // Esta solución es dinámica según el tamaño del canvas del contexto que nos pasan
        const aspectRatio = this.background.width / this.background.height
        const ctxCanvasHeight = context.canvas.height
        context.drawImage(this.background, offset, 0, ctxCanvasHeight * aspectRatio, ctxCanvasHeight)
    }
}

