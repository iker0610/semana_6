import Level from './Level.js';
import SpriteSheet from './SpriteSheet.js'
import {createBackgroundLayer} from './layers.js';

export function loadImage(url) {
    // código del ejercicio 3
    return new Promise(resolve => {
        // Generamos una imagen
        const image = new Image()
        // Añadimos los listener antes de indicar la URI de la imagen para que la imagen no pueda cargarse antes de tiempo
        image.addEventListener('load', () => resolve(image))
        // Indicamos cuál es la URI de la imagen
        image.src = url
    })
}

function loadJSON(url) {
    // código del ejercicio 2
    return fetch(url).then(response => response.json())
}


function createTiles(level, backgrounds) {
    // código del ejercicio 5
    const createNumSequence = (start, size) => Array.from(Array(size), (_, index) => index + start)

    backgrounds.forEach(({tile, ranges}) => {
        ranges.forEach(range => {
            const [start_x, width, start_y, height] = range
            createNumSequence(start_x, width).forEach(x =>
                createNumSequence(start_y, height).forEach(y =>
                    level.tiles.set(x, y, {name: tile,})
                )
            )
        })
    })
}


function loadSpriteSheet() {
    // código del ejercicio 4;
    return loadJSON('/sprites/sprites.json')
        .then(sheetSpec => Promise.all([sheetSpec, loadImage(sheetSpec['imageURL'])]))
        .then(([sheetSpec, image]) => {
            const sprites = new SpriteSheet(image, sheetSpec['tileW'], sheetSpec['tileH'])
            sheetSpec['tiles'].forEach(({name, index: [x, y]}) => {
                sprites.defineTile(name, x, y)
            })
            return sprites
        })
}


export function loadLevel() {
    return loadJSON('/levels/level.json') // qué tiles hay que poner y dónde dentro de este nivel
        .then(levelSpec => Promise.all([
            levelSpec,
            loadSpriteSheet(), // cargar imágenes de un spritesheet como sprites
        ]))
        .then(([levelSpec, backgroundSprites]) => {
            const level = new Level();
            createTiles(level, levelSpec['backgrounds']); // desplegar tiles en la estrucura Matrix

            // cargar canvas
            level.background = createBackgroundLayer(level, backgroundSprites); // canvas buffer

            return level;
        });
}
