import { Level } from './Level.js';
import {SpriteSheet} from './SpriteSheet.js';
export function loadImage(url) {
    // código del ejercicio 3
    return new Promise(resolve => {
        // Generamos una imagen
        const image = new Image();
        // Añadimos los listener antes de indicar la URI de la imagen para que la imagen no pueda cargarse antes de tiempo
        image.addEventListener('load', () => resolve(image));
        // Indicamos cuál es la URI de la imagen
        image.src = url;
    });
}
async function loadJSON(url) {
    // código del ejercicio 2
    const response = await fetch(url);
    return await response.json();
}
function loadSpriteSheet() {
    // código del ejercicio 4;
    return loadJSON('/sprites/sprites.json')
        .then((sheetSpec) => Promise.all([sheetSpec, loadImage(sheetSpec['imageURL'])]))
        .then(([sheetSpec, image]) => {
        const sprites = new SpriteSheet(image, sheetSpec.tileW, sheetSpec.tileH);
        sheetSpec.tiles.forEach(({ name, index: [x, y] }) => {
            sprites.defineTile(name, x, y);
        });
        return sprites;
    });
}
export function loadLevel() {
    return loadJSON('/levels/level.json') // qué tiles hay que poner y dónde dentro de este nivel
        // cargar imágenes de un spritesheet como sprites
        .then(levelSpec => Promise.all([levelSpec, loadSpriteSheet()]))
        .then(([levelSpec, backgroundSprites]) => {
        // cargar canvas y crear nivel
        return new Level(levelSpec, backgroundSprites);
    });
}
