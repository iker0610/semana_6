export class Matrix {
    constructor() {
        this.grid = [];
    }
    forEach(callback) {
        this.grid.forEach((column, x) => {
            column.forEach((value, y) => {
                callback(value, x, y);
            });
        });
    }
    get(x, y) {
        try {
            return this.grid[x][y];
        }
        catch (_) {
            return undefined;
        }
    }
    set(x, y, value) {
        if (!this.grid[x])
            this.grid[x] = [];
        this.grid[x][y] = value;
    }
}
