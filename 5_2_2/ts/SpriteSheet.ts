export class SpriteSheet {
    private readonly image: HTMLImageElement
    private readonly width: number
    private readonly height: number
    private tiles: Map<string, HTMLCanvasElement>

    constructor(image: HTMLImageElement, width: number, height: number) {
        this.image = image
        this.width = width
        this.height = height
        this.tiles = new Map()
    }

    define(name: string, x: number, y: number, width: number, height: number) {
        const buffer = document.createElement('canvas')
        buffer.width = width
        buffer.height = height

        // Ejercicio 7 (Tema: Canvas)
        // pintar en el buffer el tile situado en x, y dentro de la hoja de sprites this.image
        buffer.getContext('2d')!.drawImage(this.image, x, y, width, height, 0, 0, buffer.width, buffer.height)

        this.tiles.set(name, buffer)
    }

    defineTile(name: string, x: number, y: number) {
        this.define(name, x * this.width, y * this.height, this.width, this.height)
    }

    draw(name: string, context: CanvasRenderingContext2D, x: number, y: number) {
        const buffer = this.tiles.get(name)
        if (!buffer) throw new Error(`El tile ${name} no está inicializado aún.`)
        context.drawImage(buffer, x, y)
    }

    drawTile(name: string, context: CanvasRenderingContext2D, x: number, y: number) {
        this.draw(name, context, x * this.width, y * this.height)
    }
}


