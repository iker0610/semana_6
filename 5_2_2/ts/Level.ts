import {Matrix} from './math.js'
import {SpriteSheet} from "./SpriteSheet.js"
import {BackgroundSpec, LevelSpec, RangeSpec} from "./DataStructures"

type LevelTileData = { name: string }

export class Level {
    readonly tiles: Matrix<LevelTileData> = new Matrix()
    background: HTMLCanvasElement

    constructor(levelBackgrounds: LevelSpec, backgroundSprites: SpriteSheet) {
        // Con los sprites generamos el background
        this.loadTiles(levelBackgrounds.backgrounds) // desplegar tiles en la estrucura Matrix
        this.background = this.createBackgroundLayer(backgroundSprites)
    }

    createBackgroundLayer(this: Level, sprites: SpriteSheet) {
        const buffer = document.createElement('canvas') // 300 x 300
        buffer.width = 2048
        buffer.height = 240
        const context = buffer.getContext("2d")!

        // ejercicio 8 (Tema 5: Canvas)
        // Por cada tile del level situado en x, y
        // dibujar dicho tile en el contexto de buffer, haciendo uso del método drawTile del objeto sprites
        this.tiles.forEach((tile, x, y) => sprites.drawTile(tile.name, context, x, y))

        return buffer
    }


    draw(context: CanvasRenderingContext2D, offset: number) {
        /*
         Ejercicio 9. Tema 5: Canvas
         dibujar en el contexto la imagen de background con el
         desplazamiento indicado en el parámetro offset
         (recuerda que el contexto tiene unas dimensiones de 300x300)
         */

        // Esta solución es dinámica según el tamaño del canvas del contexto que nos pasan
        const aspectRatio = this.background.width / this.background.height
        const ctxCanvasHeight = context.canvas.height
        context.drawImage(this.background, offset, 0, ctxCanvasHeight * aspectRatio, ctxCanvasHeight)
    }

    private loadTiles(backgrounds: BackgroundSpec[]) {
        // código del ejercicio 5
        const createNumSequence = (start: number, size: number) => Array.from(Array(size), (_, index) => index + start)

        backgrounds.forEach(({tile, ranges}) => {
            ranges.forEach((range: RangeSpec) => {
                const [start_x, width, start_y, height] = range
                createNumSequence(start_x, width).forEach(x =>
                    createNumSequence(start_y, height).forEach(y =>
                        this.tiles.set(x, y, {name: tile}),
                    ),
                )
            })
        })
    }
}

