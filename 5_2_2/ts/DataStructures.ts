//-----------------------------------------------------------------
//  Level related Interfaces

interface LevelSpec {
    backgrounds: BackgroundSpec[]
}

interface BackgroundSpec {
    tile: string
    ranges: RangeSpec[]
}

type RangeSpec = [start_x: number, width: number, start_y: number, height: number]


//-----------------------------------------------------------------
//  Sprites related Interfaces

interface SpriteSheetSpec {
    imageURL: string
    tileW: number
    tileH: number
    tiles: TileSpec[]
}

interface TileSpec {
    name: string
    index: [x: number, y: number]
}

export {LevelSpec, BackgroundSpec, RangeSpec, SpriteSheetSpec, TileSpec}