export class Matrix<ValueType> {
    private readonly grid: ValueType[][] = []

    forEach(callback: (value: ValueType, x: number, y: number) => void) {
        this.grid.forEach((column, x) => {
            column.forEach((value, y) => {
                callback(value, x, y)
            })
        })
    }

    get(x: number, y: number): ValueType | undefined {
        try { return this.grid[x][y] } catch (_) { return undefined }
    }

    set(x: number, y: number, value: ValueType) {
        if (!this.grid[x]) this.grid[x] = []
        this.grid[x][y] = value
    }
}
